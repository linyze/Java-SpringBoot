/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : secondhand

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2022-03-10 11:25:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `a_Account` varchar(100) NOT NULL,
  `a_Address1` varchar(100) NOT NULL,
  `a_Address2` varchar(100) DEFAULT NULL,
  `a_Address3` varchar(100) DEFAULT NULL,
  `a_Address4` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of address
-- ----------------------------
INSERT INTO `address` VALUES ('1003', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区');
INSERT INTO `address` VALUES ('1004', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学北校区');
INSERT INTO `address` VALUES ('1005', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区');
INSERT INTO `address` VALUES ('1006', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区');
INSERT INTO `address` VALUES ('1007', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区');
INSERT INTO `address` VALUES ('1008', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区');
INSERT INTO `address` VALUES ('1009', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区');
INSERT INTO `address` VALUES ('1010', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区');
INSERT INTO `address` VALUES ('1001', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区');
INSERT INTO `address` VALUES ('1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区', '贵州省贵阳市贵州大学东校区');
INSERT INTO `address` VALUES ('1015', '无', '无', '无', '无');
INSERT INTO `address` VALUES ('admin', '无', '无', '无', '无');

-- ----------------------------
-- Table structure for attribute
-- ----------------------------
DROP TABLE IF EXISTS `attribute`;
CREATE TABLE `attribute` (
  `a_Account` varchar(100) NOT NULL,
  `p_Id` varchar(100) DEFAULT NULL,
  `a_Brand` varchar(100) DEFAULT NULL,
  `a_BuyTime` date DEFAULT NULL,
  `a_view` varchar(100) DEFAULT NULL,
  `a_Value` double DEFAULT NULL,
  PRIMARY KEY (`a_Account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of attribute
-- ----------------------------

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `c_Id` varchar(100) NOT NULL,
  `c_Name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`c_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('c01', '笔记本电脑');
INSERT INTO `category` VALUES ('c02', '台式电脑');
INSERT INTO `category` VALUES ('c03', '平板电脑');
INSERT INTO `category` VALUES ('c04', '笔记本');
INSERT INTO `category` VALUES ('c05', '男士手表');
INSERT INTO `category` VALUES ('c06', '雨伞');
INSERT INTO `category` VALUES ('c07', '太阳伞');
INSERT INTO `category` VALUES ('c08', '太阳眼镜');
INSERT INTO `category` VALUES ('c09', '足球');
INSERT INTO `category` VALUES ('c10', '篮球');
INSERT INTO `category` VALUES ('c11', '乒乓球');
INSERT INTO `category` VALUES ('c12', '网球');
INSERT INTO `category` VALUES ('c13', '网球拍');
INSERT INTO `category` VALUES ('c14', '乒乓球拍');
INSERT INTO `category` VALUES ('c15', '足球鞋');
INSERT INTO `category` VALUES ('c16', '篮球鞋');
INSERT INTO `category` VALUES ('c17', '休闲鞋');
INSERT INTO `category` VALUES ('c18', '板鞋');
INSERT INTO `category` VALUES ('c19', '女士手提包');
INSERT INTO `category` VALUES ('c20', '冰箱');
INSERT INTO `category` VALUES ('c21', '沙发');
INSERT INTO `category` VALUES ('c22', '手机');
INSERT INTO `category` VALUES ('c23', '相机');
INSERT INTO `category` VALUES ('c24', '键盘');
INSERT INTO `category` VALUES ('c25', '鼠标');
INSERT INTO `category` VALUES ('c26', '屏幕');
INSERT INTO `category` VALUES ('c27', '旅行箱');
INSERT INTO `category` VALUES ('c28', '屏幕');
INSERT INTO `category` VALUES ('c29', '图书');
INSERT INTO `category` VALUES ('c30', '音乐专辑');
INSERT INTO `category` VALUES ('c31', '女士手表');
INSERT INTO `category` VALUES ('c33', '女装');
INSERT INTO `category` VALUES ('c34', '男装');
INSERT INTO `category` VALUES ('c35', '书包');
INSERT INTO `category` VALUES ('c36', '手办');
INSERT INTO `category` VALUES ('c37', '游戏手柄');
INSERT INTO `category` VALUES ('c38', '游戏机');
INSERT INTO `category` VALUES ('c39', '杯子');
INSERT INTO `category` VALUES ('c40', '床上桌');

-- ----------------------------
-- Table structure for order2
-- ----------------------------
DROP TABLE IF EXISTS `order2`;
CREATE TABLE `order2` (
  `o_Id` varchar(100) NOT NULL,
  `o_ItemId` int(255) DEFAULT NULL,
  `o_Seller` varchar(100) DEFAULT NULL,
  `o_Buyer` varchar(100) DEFAULT NULL,
  `o_Baddress` varchar(100) DEFAULT NULL,
  `o_Saddress` varchar(100) DEFAULT NULL,
  `o_Date` date DEFAULT NULL,
  `o_Status` varchar(100) DEFAULT '0',
  PRIMARY KEY (`o_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order2
-- ----------------------------
INSERT INTO `order2` VALUES ('o01', '90001', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '1');
INSERT INTO `order2` VALUES ('o02', '90002', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '1');
INSERT INTO `order2` VALUES ('o03', '90003', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '0');
INSERT INTO `order2` VALUES ('o04', '90004', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '3');
INSERT INTO `order2` VALUES ('o05', '90005', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '1');
INSERT INTO `order2` VALUES ('o06', '90005', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '0');
INSERT INTO `order2` VALUES ('o07', '90007', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '3');
INSERT INTO `order2` VALUES ('o08', '90008', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '0');
INSERT INTO `order2` VALUES ('o09', '90009', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '1');
INSERT INTO `order2` VALUES ('o10', '90010', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '1');
INSERT INTO `order2` VALUES ('o11', '90006', '1001', '1002', '贵州省贵阳市贵州大学东校区', null, null, '0');
INSERT INTO `order2` VALUES ('o12', '90001', '1001', '1001', '贵州省贵阳市贵州大学北校区', null, null, '0');
INSERT INTO `order2` VALUES ('o13', '90007', '1001', '1001', '贵州省贵阳市贵州大学南校区', null, null, '0');
INSERT INTO `order2` VALUES ('o14', '90001', '1001', '1001', '贵州省贵阳市贵州大学西校区', null, null, '0');
INSERT INTO `order2` VALUES ('o15', '90002', '1001', '1001', '贵州省贵阳市贵州大学东校区', null, null, '0');
INSERT INTO `order2` VALUES ('o16', '90002', '1001', '1001', '贵州省贵阳市贵州大学东校区', null, null, '0');
INSERT INTO `order2` VALUES ('o17', '90002', '1001', '1001', '贵州省贵阳市贵州大学南校区', null, null, '0');
INSERT INTO `order2` VALUES ('o18', '90001', '1001', '1001', '贵州省贵阳市贵州大学西校区', null, null, '0');
INSERT INTO `order2` VALUES ('o19', '90002', '1001', '1001', '贵州省贵阳市贵州大学南校区', null, null, '0');
INSERT INTO `order2` VALUES ('o20', '90002', '1001', 'admin', '无', null, null, '0');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `p_Id` int(100) NOT NULL AUTO_INCREMENT,
  `p_Account` varchar(100) DEFAULT NULL,
  `p_Name` varchar(100) DEFAULT NULL,
  `c_Id` varchar(100) DEFAULT NULL,
  `p_Title` varchar(100) DEFAULT NULL,
  `p_Des` varchar(100) DEFAULT NULL,
  `p_Price` double DEFAULT NULL,
  `p_Date` date DEFAULT NULL,
  `p_href1` varchar(255) DEFAULT NULL,
  `p_href` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`p_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=90018 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('90001', '1001', '屏幕', 'c26', '9成新飞利浦27英寸显示器', '27英寸,16:9屏幕比例，60hz刷新频率', '2000', '2020-12-01', null, '/images/product/90001.jpg');
INSERT INTO `product` VALUES ('90002', '1001', '笔记本电脑', 'c01', '9成新暗影精灵4笔记本电脑', '16.5英寸屏幕,16gb运行内存，1TB+258GB硬盘组合，i5', '3500', null, null, '/images/product/90002.jpg');
INSERT INTO `product` VALUES ('90003', '1001', '雨伞', 'c06', '全新雨伞，买来没用过', '伞比较大，能撑三个人', '10', null, null, '/images/product/90003.jpg');
INSERT INTO `product` VALUES ('90004', '1001', '手机', 'c22', '8成新华为P40', '256g储存内存，外观背壳有点磨损，不仔细看的话看不出', '3000', null, null, '/images/product/90004.jpg');
INSERT INTO `product` VALUES ('90005', '1001', '书籍', 'c29', '深入理解计算机系统，全新', '全新，膜都没撕', '20', null, null, '/images/product/90005.jpg');
INSERT INTO `product` VALUES ('90006', '1001', '音乐专辑', 'c30', '林俊杰《幸存者·如你》实体专辑，全新，便宜出', '基本算是全新，就拆开来看过几次', '100', null, null, '/images/product/90006.jpg');
INSERT INTO `product` VALUES ('90007', '1001', '音乐专辑', 'c30', '林俊杰《伟大的渺小》黑胶专辑，全新，便宜出', '基本算是全新，就拆开来看过几次。歌词本保存完好', '200', null, null, '/images/product/90007.jpg');
INSERT INTO `product` VALUES ('90008', '1001', '键盘', 'c24', '8成新cherry键盘', '8成新cherry键盘，红轴', '300', null, null, '/images/product/90008.jpg');
INSERT INTO `product` VALUES ('90009', '1001', '相机', 'c23', '8成新Sony ILCE-6400L', '8成新，外观有磨损，不影响使用', '5000', null, null, '/images/product/90009.jpg');
INSERT INTO `product` VALUES ('90010', '1001', '耳机', 'c32', '9成新sony无线耳机', '9成新，外观完美。', '500', null, null, '/images/product/90010.jpg');
INSERT INTO `product` VALUES ('90011', '1002', '屏幕', 'c26', '9成三星31.5英寸4K显示器', '31.5英寸,16:9屏幕比例，60hz刷新频率', '1500', null, null, '/images/product/90011.jpg');
INSERT INTO `product` VALUES ('90012', '1002', '笔记本电脑', 'c01', '9成新华硕VIvoBook14', '16.5英寸屏幕,16gb运行内存，1TB+258GB硬盘组合，i5', '3599', null, null, '/images/product/90012.jpg');
INSERT INTO `product` VALUES ('90013', '1002', '雨伞', 'c06', '天堂伞大号', '伞比较大，能撑三个人', '10', null, null, '/images/product/90013.jpg');
INSERT INTO `product` VALUES ('90014', '1002', '手机', 'c22', '8成新iphone11无锁', '256g储存内存，外观背壳有点磨损，不仔细看的话看不出', '3000', null, null, '/images/product/90014.png');
INSERT INTO `product` VALUES ('90015', '1002', '图书', 'c29', '深入理解计算机系统，全新', '全新，膜都没撕', '20', null, null, '/images/product/90015.jpg');
INSERT INTO `product` VALUES ('90016', '1002', '音乐专辑', 'c30', '林俊杰《幸存者·如你》实体专辑，全新，便宜出', '基本算是全新，就拆开来看过几次', '99', null, null, '/images/product/90016.png');
INSERT INTO `product` VALUES ('90017', '1002', '音乐专辑', 'c30', '林俊杰《伟大的渺小》黑胶专辑，全新，便宜出', '基本算是全新，就拆开来看过几次。歌词本保存完好', '198', null, null, '/images/product/90017.jpg');

-- ----------------------------
-- Table structure for shoppingcar
-- ----------------------------
DROP TABLE IF EXISTS `shoppingcar`;
CREATE TABLE `shoppingcar` (
  `s_Id` int(100) NOT NULL AUTO_INCREMENT,
  `u_Account` varchar(255) DEFAULT NULL,
  `p_Id` int(100) DEFAULT NULL,
  PRIMARY KEY (`s_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shoppingcar
-- ----------------------------
INSERT INTO `shoppingcar` VALUES ('2', '1001', '90002');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `u_Account` varchar(100) NOT NULL,
  `u_Name` varchar(100) DEFAULT NULL,
  `u_Password` varchar(100) DEFAULT NULL,
  `u_Sex` varchar(10) DEFAULT NULL,
  `u_Email` varchar(100) DEFAULT NULL,
  `u_Phone` varchar(100) DEFAULT NULL,
  `u_Url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`u_Account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1001', '小明', 'qweasd123', 'nan', '123456@qq.com', '12345678910', null);
INSERT INTO `user` VALUES ('1002', '小红', 'qweasd123', '男', '123456@qq.com', '12345678910', null);
INSERT INTO `user` VALUES ('1003', '小蓝', 'qweasd123', '男', '123456@qq.com', '12345678910', null);
INSERT INTO `user` VALUES ('1004', '小绿', 'qweasd123', '男', '123456@qq.com', '12345678910', null);
INSERT INTO `user` VALUES ('1005', '小紫', 'qweasd123', '男', '123456@qq.com', '12345678910', null);
INSERT INTO `user` VALUES ('1006', '小黄', 'qweasd123', '男', '123456@qq.com', '12345678910', null);
INSERT INTO `user` VALUES ('1007', '小橙', 'qweasd123', '男', '123456@qq.com', '12345678910', null);
INSERT INTO `user` VALUES ('1008', '大明', 'qweasd123', '男', '123456@qq.com', '12345678910', null);
INSERT INTO `user` VALUES ('1009', '张三', 'qweasd123', '男', '123456@qq.com', '12345678910', null);
INSERT INTO `user` VALUES ('1010', '李四', 'qweasd123', '男', '123456@qq.com', '12345678910', null);
INSERT INTO `user` VALUES ('1011', '巴乐水', 'qweasd123', '男', '123456@qq.com', '12345678910', null);
INSERT INTO `user` VALUES ('1012', '尤白梅', 'qweasd123', '男', '123456@qq.com', '12345678910', null);
INSERT INTO `user` VALUES ('1013', '祭致', 'qweasd123', '男', '123456@qq.com', '12345678910', null);
INSERT INTO `user` VALUES ('1014', '玉凡', 'qweasd123', '男', '123456@qq.com', '12345678910', null);
INSERT INTO `user` VALUES ('1015', '宇子', 'qweasd123', '男', '123456@qq.com', '12345678910', null);
INSERT INTO `user` VALUES ('admin', '洛水', 'qweasd123', null, null, null, null);
